<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id',8);
            $table->string('fastone_number',8);
            $table->string('email',100)->unique();
            $table->string('password',200);
            $table->tinyInteger('user_type');
            $table->tinyInteger('is_active');
            $table->tinyInteger('account_status');
            $table->tinyInteger('from_iac');
            $table->string('iac_number',100)->nullable();
            $table->rememberToken();
            $table->timestamp('last_login');
            $table->timestamps();
            $table->tinyInteger('delete_flag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
