<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bank_name',255);
            $table->string('vendor_name',255);
            $table->integer('country_id')->unsigned()->index();
//            $table->foreign('country_id')->references('id')->on('countries');
            $table->integer('currency_id')->unsigned()->index();
//            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->float('min_in');
            $table->float('max_in');
            $table->float('min_out');
            $table->float('max_out');
            $table->float('fee');
            $table->float('min_fee');
            $table->integer('priority');
            $table->tinyInteger('in_status');
            $table->tinyInteger('out_status');
            $table->timestamps();
            $table->integer('fee_type');
            $table->string('required_list')->nullable();
            $table->tinyInteger('delete_flag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fis');
    }
}
