<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBusinessInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_business_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
//            $table->foreign('user_id')->references('id')->on('users');
            $table->string('corporate_name',30);
            $table->string('corporate_address',255);
            $table->string('registration_number',30);
            $table->string('bussiness_type',30);
            $table->string('website',100)->nullable();
            $table->string('owner_name',30)->nullable();
            $table->string('owner_country',30)->nullable();
            $table->date('owner_birthday')->nullable();
            $table->tinyInteger('director_verified_status');
            $table->tinyInteger('kyc_corporate_status');
            $table->string('kyc_review_note',255);
            $table->string('kyc_owner_identity_url',255);
            $table->tinyInteger('kyc_owner_identity_status');
            $table->string('kyc_owner_address_url,255');
            $table->tinyInteger('kyc_owner_address_status');
            $table->string('kyc_corporate_identity_url',255);
            $table->tinyInteger('kyc_corporate_identity_status');
            $table->string('kyc_corporate_address_url',255);
            $table->tinyInteger('kyc_corporate_address_status');
            $table->timestamps();
            $table->tinyInteger('delete_flag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_business_infos');
    }
}
