<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFiRequiredFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fi_required_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fi_id')->unsigned()->index();
//            $table->foreign('fi_id')->references('id')->on('fis');
            $table->string('name',255);
            $table->tinyInteger('status');
            $table->tinyInteger('type');
            $table->string('rule',255);
            $table->timestamps();
            $table->tinyInteger('delete_flag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fi_required_fields');
    }
}
