<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBusinessInfos extends Model
{

    public function User()
    {
        return $this->belongsTo('App\User');
    }

    public function Countries()
    {
        return $this->belongsTo('App\Countries');
    }
}
