<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferBusinessInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfer_business_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transfer_id')->unsigned()->index();
//            $table->foreign('transfer_id')->references('id')->on('transfers');
            $table->string('company_name',100);
            $table->string('registration_number',50);
            $table->string('postal_code',20)->nullable();
            $table->integer('state_id')->unsigned()->nullable();
//            $table->foreign('state_id')->references('id')->on('states');
            $table->string('town',100)->nullable();
            $table->string('street',255)->nullable();
            $table->timestamps();
            $table->tinyInteger('delete_flag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfer_business_infos');
    }
}
