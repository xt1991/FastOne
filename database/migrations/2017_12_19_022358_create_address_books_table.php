<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address_books', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
//            $table->foreign('user_id')->references('id')->on('users');
            $table->string('first_name',20);
            $table->string('last_name',20);
            $table->string('middle_name',20)->nullable();
            $table->string('email',100);
            $table->string('postal_code',20)->nullable();
            $table->integer('state_id')->unsigned()->nullable();
//            $table->foreign('state_id')->references('id')->on('states');
            $table->string('town',100)->nullable();
            $table->string('street',255)->nullable();
            $table->string('purpose',255);
            $table->string('document_files_url',255);
            $table->text('bank_account_detail')->nullable();
            $table->timestamps();
            $table->tinyInteger('delete_flag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address_books');
    }
}
