<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPersonalInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_personal_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
//            $table->foreign('user_id')->references('id')->on('users');
            $table->string('first_name',30);
            $table->string('last_name',30);
            $table->string('middle_name',30);
            $table->date('date_of_birth');
            $table->string('phone',30);
            $table->integer('country_id')->unsigned()->index();
//            $table->foreign('country_id')->references('id')->on('countries');
            $table->string('address',100);
            $table->tinyInteger('kyc_personal_status');
            $table->string('kyc_review_note',255);
            $table->string('kyc_identity_url',255);
            $table->tinyInteger('kyc_identity_status');
            $table->string('kyc_address_url',255);
            $table->tinyInteger('kyc_address_status');
            $table->timestamps();
            $table->tinyInteger('delete_flag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('user_personal_infos');
    }
}
