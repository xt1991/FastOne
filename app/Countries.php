<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    public function UserBusinessInfos()
    {
        return $this->hasOne('App\UserBusinessInfos');
    }
}
