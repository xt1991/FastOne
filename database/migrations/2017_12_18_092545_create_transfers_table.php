<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
//            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('fi_id')->unsigned()->index();
//            $table->foreign('fi_id')->references('id')->on('fi');
            $table->string('transfer_no',12)->nullable();
            $table->string('ref no',12);
            $table->tinyInteger('receive_money_type');
            $table->integer('from_country_id')->unsigned()->index();
//            $table->foreign('from_country_id')->references('id')->on('countries');
            $table->integer('to_country_id')->unsigned()->index();
//            $table->foreign('to_country_id')->references('id')->on('countries');
            $table->tinyInteger('sending_method');
            $table->tinyInteger('payment_method');
            $table->integer('from_currency_id')->unsigned()->index();
//            $table->foreign('from_currency_id')->references('id')->on('currencies');
            $table->integer('to_currency_id')->unsigned()->index();
//            $table->foreign('to_currency_id')->references('id')->on('currencies');
            $table->string('first_name',20);
            $table->string('last_name',20);
            $table->string('middle_name',20)->nullable();
            $table->string('email',100);
            $table->string('postal_code',20)->nullable();
            $table->integer('state_id')->unsigned()->nullable();
//            $table->foreign('state_id')->references('id')->on('states');
            $table->string('town',100)->nullable();
            $table->string('street',255)->nullable();
            $table->string('purpose',255);
            $table->string('document_files_url',255);
            $table->text('bank_account_detail')->nullable();
            $table->float('from_amount');
            $table->float('to_amount');
            $table->float('real_f1_receive_amount')->nullable();
            $table->float('real_destination_receive_amount')->nullable();
            $table->float('real_f1_receive_exchange_rate')->nullable();
            $table->float('real_destination_receive_exchange_rate')->nullable();
            $table->float('submit_exchange_rate');
            $table->float('fee');
            $table->date('f1_receive_money_date')->nullable();
            $table->date('destination_receive_date')->nullable();
            $table->string('note',255)->nullable();
            $table->tinyInteger('status');
            $table->timestamps();
            $table->tinyInteger('delete_flag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfers');
    }
}
